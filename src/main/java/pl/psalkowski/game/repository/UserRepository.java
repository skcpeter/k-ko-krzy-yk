package pl.psalkowski.game.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import pl.psalkowski.game.model.Room;
import pl.psalkowski.game.model.User;

@Transactional(readOnly=false)
public interface UserRepository extends JpaRepository<User, String> {

    List<User> findByNick(String nick);
    List<User> findByStatus(int status);
	List<User> findByRoom(Room room);
}
