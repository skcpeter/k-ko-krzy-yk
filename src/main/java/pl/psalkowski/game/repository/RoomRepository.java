package pl.psalkowski.game.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import pl.psalkowski.game.model.Room;

@Transactional(readOnly=false)
public interface RoomRepository extends JpaRepository<Room, Integer> {
	
	List<Room> findByName(String name);
}
