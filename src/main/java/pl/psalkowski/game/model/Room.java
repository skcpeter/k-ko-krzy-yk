package pl.psalkowski.game.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

import scala.annotation.varargs;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Room")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="id")
public class Room {
	public final static int OWNER = 0;
	public final static int GUEST = 1;
	public final static int DRAW = 2;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "room_id")
	private Integer id;
	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="room")
	private List<User> users;
	private String name;
	private int map[][];
	private int now = -1;
	
	/**
	 * -1 -- defauult
	 * 0 -- wygra� owner
	 * 1 -- wygra� go��
	 * 2 -- remis
	 */
	private int score = -1;

	public Room() {
		this.map = new int[3][3];
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				this.map[i][j] = -1;
			}
		}
		this.users = new ArrayList<User>();
	}

	public boolean isFull() {
		return users.size() == 2;
	}

	public int nextTurn() {
		return (this.now = this.now == OWNER ? GUEST : OWNER);
	}

	public int getNow() {
		return now;
	}

	public void setNow(int now) {
		this.now = now;
	}
	
	public void setSelect(int x, int y) {
		map[x][y] = this.now;
	}

	public int[][] getMap() {
		return this.map;
	}

	public void setMap(int[][] map) {
		this.map = map;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addUser(User user) {
        this.users.add(user);
        if (user.getRoom() != this) {
        	user.setRoom(this);
        }
    }
	
	public List<User> getUsers() {
        return this.users;
    }
	
	public void removeUser(User user) {
		if(this.users.contains(user)) {
			this.users.remove(user);
		}
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public boolean isEndGame() {
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(this.map[i][j] == -1)
					return false;
			}
		}
		
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
