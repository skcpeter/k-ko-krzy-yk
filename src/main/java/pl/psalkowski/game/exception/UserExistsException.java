package pl.psalkowski.game.exception;

public class UserExistsException extends RuntimeException {

	private static final long serialVersionUID = -624058618610540712L;

	public UserExistsException(String message) {
		super(message);
	}
}
