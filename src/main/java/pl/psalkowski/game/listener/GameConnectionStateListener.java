package pl.psalkowski.game.listener;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.bristleback.server.bristle.api.ConnectionStateListener;
import pl.bristleback.server.bristle.listener.ConnectionStateListenerChain;
import pl.psalkowski.game.action.client.RoomClientAction;
import pl.psalkowski.game.action.client.UserClientAction;
import pl.psalkowski.game.model.Room;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.repository.UserRepository;
import pl.psalkowski.game.service.RoomService;
import pl.psalkowski.game.service.UserService;

@Component
public class GameConnectionStateListener implements
		ConnectionStateListener<User> {

	private static Logger log = Logger.getLogger(GameConnectionStateListener.class);
	
	@Autowired
	private UserClientAction userClientAction;
	
	@Autowired
	private RoomClientAction roomClientAction;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoomService roomService;

	@Autowired
	private UserRepository userRepository;

	@Override
	public void userConnected(User user,
			ConnectionStateListenerChain connectionStateListenerChain) {
	}

	@Override
	public void userDisconnected(User user,
			ConnectionStateListenerChain connectionStateListenerChain) {

		log.info("U�ytkownik opu�ci� gr�: " + user.getNick());
		if(userRepository.exists(user.getId())) {
			log.info("Usuwanie u�ytkownika");
			
			Room room = user.getRoom();
			userService.removeUser(user);
			
			if(room != null) {
				room = roomService.find(room.getId());
				roomClientAction.updateLobby(room);
			}
			
			userClientAction.removeUser(user);
		} else {
			log.info("U�ytkownik nie istnieje: " + user.getNick());
		}
	}

}
