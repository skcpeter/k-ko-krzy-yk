package pl.psalkowski.game.action;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import pl.bristleback.server.bristle.api.annotations.Action;
import pl.bristleback.server.bristle.api.annotations.ActionClass;
import pl.psalkowski.game.action.client.GameEngineClientAction;
import pl.psalkowski.game.action.client.RoomClientAction;
import pl.psalkowski.game.engine.GameEngine;
import pl.psalkowski.game.model.Room;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.repository.UserRepository;
import pl.psalkowski.game.service.RoomService;

@Controller
@ActionClass(name="GameEngineAction")
public class GameEngineAction {
	
	private static Logger log = Logger.getLogger(GameEngineAction.class);
	
	@Autowired
	private RoomService roomService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private GameEngine game;
	
	@Autowired
	private GameEngineClientAction clientAction;
	
	@Autowired
	private RoomClientAction roomClientAction;
	
	@Action
	public Void startGame(int roomId) {
		Room room = roomService.find(roomId);
		
		game.startGame(room);
		clientAction.startGame(room);
		return null;
	}
	
	@Action
	public Void hit(int row, int col, int roomId) {
		Room room = roomService.find(roomId);
		room = game.hit(row, col, room);
		
		clientAction.hit(room);
		if(room.getScore() >= 0) {
			clientAction.endGame(room);
			roomClientAction.updateUsersScore(room);
		}
		
		return null;
	}
}
