package pl.psalkowski.game.action;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import pl.bristleback.server.bristle.api.annotations.Action;
import pl.bristleback.server.bristle.api.annotations.ActionClass;
import pl.psalkowski.game.action.client.ChatClientAction;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.service.RoomService;

@Controller
@ActionClass(name="ChatAction")
public class ChatAction {
	
	private static Logger log = Logger.getLogger(ChatAction.class);

	@Autowired
	private RoomService roomService;
	
	@Autowired
	private ChatClientAction chatClient;
	
	@Action
	public Void sendMessage(String message, User user) {
		chatClient.addMessage(message, user);
		return null;
	}
}
