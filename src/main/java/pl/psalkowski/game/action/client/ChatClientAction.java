package pl.psalkowski.game.action.client;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.bristleback.server.bristle.api.annotations.ClientAction;
import pl.bristleback.server.bristle.api.annotations.ClientActionClass;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.service.UserService;

@Component
@ClientActionClass
public class ChatClientAction {
	
	private static Logger log = Logger.getLogger(ChatClientAction.class);

	@Autowired
	private UserService userService;
	
	
	@ClientAction
	public List<User> addMessage(String message, User user) {
		user = userService.find(user.getId());
		
		if(user.getRoom() != null) {
			log.debug("AddMessage");
			for(User user2 : userService.findByRoom(user.getRoom())) {
				log.debug(user2);
			}
			return userService.findByRoom(user.getRoom());
		}
		
		return userService.findByStatus(User.STATUS_LIVE);
	}
}
