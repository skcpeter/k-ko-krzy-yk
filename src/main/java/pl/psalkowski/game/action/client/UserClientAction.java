package pl.psalkowski.game.action.client;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.bristleback.server.bristle.api.annotations.ClientAction;
import pl.bristleback.server.bristle.api.annotations.ClientActionClass;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.repository.UserRepository;
import pl.psalkowski.game.service.UserService;

@Component
@ClientActionClass
public class UserClientAction {
	private static Logger log = Logger.getLogger(UserClientAction.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository repository;
	
	@ClientAction
	public List<User> addUser(User user) {
		return repository.findByStatus(User.STATUS_LIVE);
	}
	
	@ClientAction
	public List<User> removeUser(User user) {
		return userService.findAll();
	}
}
