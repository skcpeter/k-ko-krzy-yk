package pl.psalkowski.game.action.client;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import pl.bristleback.server.bristle.api.annotations.ClientAction;
import pl.bristleback.server.bristle.api.annotations.ClientActionClass;
import pl.psalkowski.game.model.Room;
import pl.psalkowski.game.model.User;

@Component
@ClientActionClass
public class GameEngineClientAction {
	
	private static Logger log = Logger.getLogger(GameEngineClientAction.class);

	@ClientAction
	public List<User> startGame(Room room) {
		log.debug("start game");
		return room.getUsers();
	}

	@ClientAction
	public List<User> hit(Room room) {
		return room.getUsers();
	}

	@ClientAction
	public List<User> endGame(Room room) {
		return room.getUsers();
	}
}
