package pl.psalkowski.game.action.client;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.bristleback.server.bristle.api.annotations.ClientAction;
import pl.bristleback.server.bristle.api.annotations.ClientActionClass;
import pl.psalkowski.game.model.Room;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.repository.RoomRepository;
import pl.psalkowski.game.repository.UserRepository;

@Component
@ClientActionClass
public class RoomClientAction {
	private static Logger log = Logger.getLogger(RoomClientAction.class);
	
	@Autowired
	private RoomRepository repository;
	
	@Autowired
	private UserRepository userRepository;
	
	/**
	 * Gdy zostanie utworzony nowy pok�j
	 * 
	 * @param room
	 * @return do wszystkich z lobby
	 */
	@ClientAction
	public List<User> createRoom(Room room) {
		return userRepository.findByStatus(User.STATUS_LIVE);
	}

	/**
	 * Funkcja wywo�uje si� gdy zajd� jakie� zmiany w pokoju
	 * - kto� do��czy
	 * - kto� wyjdzie
	 * - gdy jeden z u�ytkownik�w roz��czy si� z serwerem
	 * 
	 * @param room
	 * @return wszyscy u�ytkownicy, kt�rzy s� w lobby
	 */
	@ClientAction
	public List<User> updateLobby(Room room) {
		return userRepository.findByStatus(User.STATUS_LIVE);
	}

	/**
	 * Funkcja wywo�uje si� gdy jaki� u�ytkownik do��czy do pokoju
	 * 
	 * @param room
	 * @return do u�ytkownik�w, kt�rzy s� w danym pokoju
	 */
	@ClientAction
	public List<User> updateRoom(Room room) {
		return room.getUsers();
	}

	/**
	 * Wywo�uje si� przy r�cznym wychodzeniu z pokoju poprzez klikni�cie w guzik "Wyjd� z pokoju"
	 * 
	 * @param user
	 * @return do wszystkich u�ytkownik�w z pokoju (zawsze jest najwy�ej 1)
	 */
	@ClientAction
	public List<User> removeUserFromRoom(Room room) {
		return room.getUsers();
	}
	
	/**
	 * Wywo�uje si� podczas wychodzenia z pokoju, je�li w pokoju nikt nie zostanie. 
	 * 
	 * @param rooms
	 * @return do wszystkich u�ytkownik�w w lobby
	 */
	@ClientAction
	public List<User> removeRoom(List<Room> rooms) {
		return userRepository.findByStatus(User.STATUS_LIVE);
	}

	/**
	 * Chcia�bym jako parametr przekazywac room.getUsers(), 
	 * ale JSON usuwa mi rekurencje co skutkuje zwracaniem 
	 * tylko jednego u�ytkownika, a nie dw�ch
	 * 
	 * @param room
	 * @return
	 */
	@ClientAction
	public List<User> updateUsersScore(Room room) {
		return userRepository.findByStatus(User.STATUS_LIVE);
	}
}
