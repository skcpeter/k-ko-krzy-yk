package pl.psalkowski.game.action;

import org.springframework.stereotype.Controller;

import pl.bristleback.server.bristle.api.annotations.Action;
import pl.bristleback.server.bristle.api.annotations.ActionClass;

@Controller
@ActionClass(name = "KeepAlive")
public class KeepAliveAction {

	@Action
	public void ping() {
		// https://www.w3.org/Bugs/Public/show_bug.cgi?id=13104
	}
}
