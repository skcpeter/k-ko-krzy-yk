package pl.psalkowski.game.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import pl.bristleback.server.bristle.api.annotations.Action;
import pl.bristleback.server.bristle.api.annotations.ActionClass;
import pl.psalkowski.game.action.client.RoomClientAction;
import pl.psalkowski.game.model.Room;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.repository.RoomRepository;
import pl.psalkowski.game.service.RoomService;
import pl.psalkowski.game.service.UserService;

@Controller
@ActionClass(name="RoomAction")
public class RoomAction {
	
	private static Logger log = Logger.getLogger(RoomAction.class);
	
	@Autowired
	private RoomRepository roomRepository;
	
	@Autowired
	private RoomService roomService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoomClientAction roomClient;
	
	
	@Action
	public User createRoom(User owner, String name) {
		Room room = roomService.createRoom(owner, name);
		
		log.debug(room);
		roomClient.createRoom(room);
		return owner;
	}
	
	@Action
	public Room joinToRoom(User user, int roomId) {
		Room room = roomService.joinToRoom(user, roomId);
		
		roomClient.updateLobby(room);
		roomClient.updateRoom(room);
		
		this.print();
		return room;
	}
	
	@Action
	@SuppressWarnings("rawtypes")
	public Map<String, List> leaveRoom(User user) {
		Room room = roomService.removeUser(user);
		user = userService.find(user.getId());
		
		if(room == null ) {
			roomClient.removeRoom(roomService.findAll());
		} else {
			roomClient.updateLobby(room);
			roomClient.removeUserFromRoom(room);
		}
		
		Map<String, List> map = new HashMap<String, List>();
		map.put("users", userService.findAll());
		map.put("rooms", roomService.findAll());
		
		this.print();
		return map;
	}
	
	private void print() {
		log.debug("Debug data");
		for(Room room : roomService.findAll()) {
			log.debug(room);
		}
		
		for(User user : userService.findAll()) {
			log.debug(user);
		}
	}
}
