package pl.psalkowski.game.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import pl.bristleback.server.bristle.api.annotations.Action;
import pl.bristleback.server.bristle.api.annotations.ActionClass;
import pl.psalkowski.game.action.client.UserClientAction;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.repository.UserRepository;
import pl.psalkowski.game.service.RoomService;
import pl.psalkowski.game.service.UserService;

@Controller
@ActionClass(name = "JoinGame")
public class JoinGameAction {
	/**
	 * Odpowiedz serwera dostaje tylko uzytkownik, ktory wyslal wiadomosc do serwera.
	 * Zwracana wartosc zamieniana jest na JSON, wiec mozna tutaj zwrocic dowolny typ danych
	 */
	private static Logger log = Logger.getLogger(JoinGameAction.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoomService roomService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserClientAction userClient;

	@Action
	@SuppressWarnings(value = { "rawtypes" })
	public Map<String, List> join(User user,
			@NotBlank @Length(min = 2) String nick) throws Exception {
		
		log.info("U�ytkownik do��czy� do gry: " + user);
		
		user = userService.registerUser(user, nick);
		userClient.addUser(user);
		
		Map<String, List> map = new HashMap<String, List>();
		map.put("users", userService.findAll());
		map.put("rooms", roomService.findAll());
		
		return map;
	}
}
