package pl.psalkowski.game.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import pl.psalkowski.game.model.Room;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.repository.RoomRepository;
import pl.psalkowski.game.repository.UserRepository;

@Component
@Transactional(readOnly = false)
public class RoomService {
	
	private static Logger log = Logger.getLogger(RoomService.class);
	
	@Autowired
	private RoomRepository repository;
	
	@Autowired
	private UserRepository userRepository;
	
	public Room createRoom(User owner, String name) {
		Room room = new Room();
		room.setName(name);
		room = repository.saveAndFlush(room);
		
		owner.setStatus(User.STATUS_BUSY);
		room.addUser(owner);
		
		return repository.saveAndFlush(room);
	}

	public List<Room> findAll() {
		List<Room> rooms = repository.findAll();
		
		if(rooms == null) {
			return new ArrayList<Room>();
		}
		
		return rooms;
	}

	public Room find(int roomId) {
		return repository.findOne(roomId);
	}

	public Room joinToRoom(User user, int roomId) {
		Room room = repository.findOne(roomId);
		user = userRepository.findOne(user.getId());
	
		if(!room.isFull()) {
			
			
			user.setStatus(User.STATUS_BUSY);
			room.addUser(user);
			room = repository.saveAndFlush(room);
			
//			user.setRoom(room);
//			userRepository.saveAndFlush(user);
			
			return room;
		}
		
		return null;
	}

	public Room removeUser(User user) {
		user = userRepository.findOne(user.getId());
		
		Room room = user.getRoom();
		room.removeUser(user);
		
		user.setStatus(User.STATUS_LIVE);
		user.setRoom(null);
		userRepository.saveAndFlush(user);
		
		if(room.getUsers().size() == 0) {
			repository.delete(room);
			log.debug("Room have not any user");
			return null;
		}
		
		return repository.saveAndFlush(room);
	}

	public Room save(Room room) {
		return repository.saveAndFlush(room);
	}
}
