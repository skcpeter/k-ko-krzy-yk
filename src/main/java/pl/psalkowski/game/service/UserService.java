package pl.psalkowski.game.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import akka.japi.Function;
import pl.psalkowski.game.exception.UserExistsException;
import pl.psalkowski.game.model.Room;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.repository.RoomRepository;
import pl.psalkowski.game.repository.UserRepository;

@Component
@Transactional(readOnly = false)
public class UserService {
	
	private static Logger log = Logger.getLogger(UserService.class);
	
	@Autowired
	private UserRepository repository;
	
	@Autowired
	private RoomRepository roomRepository;
	
	public User registerUser(User user, String nick) throws Exception {
		if(!repository.findByNick(nick).isEmpty()) {
			throw new UserExistsException("U�ytkownik o takim nicku ju� istnieje!");
		}
		
		user.setNick(nick);
		return repository.saveAndFlush(user);
	}
	
	public void removeUser(User user) {
		if(user == null)
			return;
		
		user = repository.findOne(user.getId());
		
		Room room = user.getRoom();
		
		if(room == null) {
			repository.delete(user);
		} else {
			room.removeUser(user);
			repository.delete(user);
			
			if(room.getUsers().size() == 0) {
				room.removeUser(user);
				room = roomRepository.findOne(room.getId());
				roomRepository.delete(room);
			}
		}
	}

	public User addRoom(User user, String name) {
		Room room = new Room();
		
		user.setStatus(User.STATUS_BUSY);
		room.setName(name);
		
		return repository.saveAndFlush(user);
	}
	
	public List<User> findAll() {
		List<User> users = repository.findAll();
		
		if(users == null) {
			return new ArrayList<User>();
		}
		return users;
	}
	
	public List<User> findByStatus(int status) {
		return repository.findByStatus(status);
	}

	public User find(String id) {
		return repository.findOne(id);
	}

	public List<User> findByRoom(Room room) {
		return repository.findByRoom(room);
	}
}
