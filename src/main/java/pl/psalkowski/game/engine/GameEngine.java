package pl.psalkowski.game.engine;

import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.psalkowski.game.action.RoomAction;
import pl.psalkowski.game.model.Room;
import pl.psalkowski.game.model.User;
import pl.psalkowski.game.service.RoomService;
import pl.psalkowski.game.service.UserService;

@Component
public class GameEngine {
	
	private static Logger log = Logger.getLogger(GameEngine.class);

	@Autowired
	private RoomService roomService;
	
	@Autowired
	private UserService userService;
	
	public Room startGame(Room room) {
		this.resetGame(room);
		room.setNow(this.randomNow());
		
		return roomService.save(room);
	}

	public Room hit(int row, int col, Room room) {
		room.setSelect(row, col);
		
		int score = this.gameScore(room.getMap(), room.isEndGame());
		User owner = room.getUsers().get(0);
		User guest = room.getUsers().get(1);
		
		switch(score) {
			case 0:
				owner.setWin(owner.getWin()+1);
				guest.setLost(guest.getLost()+1);
				break;
			case 1:
				guest.setWin(guest.getWin()+1);
				owner.setLost(owner.getLost()+1);
				break;
			case 2: 
				owner.setDraw(owner.getDraw()+1);
				guest.setDraw(guest.getDraw()+1);
				break;
		}
		room.nextTurn();
		
		room.setScore(score);
		return roomService.save(room);
	}
	
	private void resetGame(Room room) {
		int[][] map = new int[3][3];
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				map[i][j] = -1;
			}
		}
		room.setMap(map);
		room.setScore(-1);
	}
	
	private int randomNow() {
		Random generator = new Random(); 
		return generator.nextInt(2);
	}
	
	private int gameScore(int[][] map, boolean end) {
		for (int i = 0; i < 3; i++) {
			if (map[i][0] == Room.OWNER && map[i][1] == Room.OWNER && map[i][2] == Room.OWNER
					|| map[0][i] == Room.OWNER && map[1][i] == Room.OWNER
					&& map[2][i] == Room.OWNER) {
				return Room.OWNER;
			}

			if (map[i][0] == Room.GUEST && map[i][1] == Room.GUEST && map[i][2] == Room.GUEST
					|| map[0][i] == Room.GUEST && map[1][i] == Room.GUEST
					&& map[2][i] == Room.GUEST) {
				return Room.GUEST;
			}
		}

		if (map[0][0] == Room.OWNER && map[1][1] == Room.OWNER && map[2][2] == Room.OWNER
				|| map[0][2] == Room.OWNER && map[1][1] == Room.OWNER
				&& map[2][0] == Room.OWNER) {
			return Room.OWNER;
		}

		if (map[0][0] == Room.GUEST && map[1][1] == Room.GUEST && map[2][2] == Room.GUEST
				|| map[0][2] == Room.GUEST && map[1][1] == Room.GUEST
				&& map[2][0] == Room.GUEST) {
			return Room.GUEST;
		}

		if(end)
			return Room.DRAW; // DRAW
		
		return -1;
	}
	
	private void print() {
		log.debug("Debug data");
		for(Room room : roomService.findAll()) {
			log.debug(room);
		}
		
		for(User user : userService.findAll()) {
			log.debug(user);
		}
	}
}
