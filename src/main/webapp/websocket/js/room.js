var Room = {
	container: '#room-content',
	turnName: '#room-content .now-turn-name',
	users: '#room-content .user-list',
	leave: '#room-content .leave-room',
	start: '#room-content .start-game',
	mapItems: '#room-content .map-item',
	mapInfo: '#room-content .map-info',

	view: {
		_tooltip: function(user) {
			return $('<div class="tooltip"><p>Wygrane: '+user.win+'</p><p>Przegrane: '+user.lost+'</p><p>Remisy: '+user.draw+'</p></div>');
		},
		_clearList: function() {
			$(Room.users).html('');
		},
		_clearMap: function() {
			$(Room.mapItems).each(function() {
				$(this).attr('class', 'map-item');
			});
		},
		_prepareWho: function(user) {
			$(Room.mapInfo).fadeOut(100,function() {
				$(this).html('Ruch gracza: <span class="now-turn-name">'+ user.nick +'</span>').fadeIn(100);
			});
		},
		_prepareEnd: function(room) {
			var msg = $('<h2/>');
			if(room.score === 0) {
				msg.html('Wygrał: ' + room.users[0].nick);
			} else if(room.score === 1) {
				msg.html('Wygrał: ' + room.users[1].nick);
			} else if(room.score === 2) {
				msg.html('Remis');
			}

			$(Room.mapInfo).fadeOut(100,function() {
				$(this).html(msg).fadeIn(100);
			});
		},
		whoNow: function(user) {
			$(Room.turnName).fadeOut(100, function() {
				 $(this).html(user.nick).fadeIn(100);
			});
		},
		addUser: function(user) {
			$(Room.users).append(
				$('<li></li>')
					.html(user.nick)
					.append(Room.view._tooltip(user))
					.attr('id', 'user-' + user.id)
						.hide().fadeIn(100)
			);
		},
		updateUsersStatus: function(users) {
			$.each(users, function(k, user) {
				var tooltip = Room.view._tooltip(user);
				$('#user-' + user.id).find('.tooltip').replaceWith(tooltip);
			});
		},
		refreshUserList: function(room, mode) {
			/**
			* dodajemy użytkowników jeśli nie ma ich na liście
			**/
			$.each(room.users, function(k, user) {
        if($('#user-'+user.id).length === 0) {
        	Room.view.addUser(user);
        }
      });

			/**
			* sprawdzamy czy jest użytkownik do usunięcia, 
			* jesli tak to go usuwamy
			**/
			$(Room.users).find('li').each(function() {
        var id = $(this).attr('id');
        var exists = false;
        $.each(room.users, function(k, user) {
          if(id === ('user-' + user.id))
            exists = true;
        });
        if(!exists) {
          $('#'+id).fadeOut(100,function() {
            $(this).remove();
          });
        }
      });
		},
		removeUser: function(user) {
			$('#user-' + user.id).fadeOut(100,function() {
				$(this).remove();
			});
		},
		// type = { tick | cross }
		hit: function(row, col, type) {
			if(type === -1) return false;
			else if(type === 0) type = 'tick';
			else if(type === 1) type = 'cross';

			$('[data-row="'+row+'"][data-col="'+col+'"]').attr('class', 'map-item map-item-'+type);
		}
	},

	bind: {
		leave: function() {
			$(Room.leave).click(function() {
				Game.serverHandler.roomActionClass.leaveRoom(Bristleback.USER_CONTEXT);
			});
		},
		start: function() {
			$(Room.start).click(function() {
				if($(Room.users).find('li').length == 2 && Room.game.started === false) {
					Game.serverHandler.gameEngineActionClass.startGame(Room.game.room);
				}
			});
		},
		map: function() {
			$(Room.mapItems).on('click', function() {
				if(Room.game.started) {
					var data = $(this).data();
					if(!$(this).hasClass('map-item-tick') && !$(this).hasClass('map-item-cross')) {
						if(Room.game.currentPlayer.id === Room.game.player.id)
							Game.serverHandler.gameEngineActionClass.hit(data.row, data.col, Room.game.room);
					}
				}
			});
		}
	},

	process: {
		server: {
			initialize: function(user) {
				Room.game.player = user;
				Room.game.room = user.room.id;

				View.changeScreen('room.html', false, function(){
					$(Room.container).data('id', user.room.id);
					Room.view.addUser(user);
					Room.bind.leave();
					Room.bind.start();
					Chat.init();
				});
			},
			joinToRoom: function(room) {
				Room.game.player = room.users[1];
				Room.game.room = room.id;

				View.changeScreen('room.html', false, function(){
					$(Room.container).data('id', room.id);
					$.each(room.users, function(k, v) {
						Room.view.addUser(v);
					});
					Room.bind.start();
					Room.bind.leave();
					Chat.init();
				});
			}
		},
		client: {
			updateRoom: function(room) {
				Room.view.refreshUserList(room);
			},
			removeUser: function(room) {
				Room.view.refreshUserList(room);
				Room.game.started = false;
				Room.game.currentPlayer = null;
			},
			startGame: function(room) {
				Room.view._clearMap();
				Room.view._prepareWho(room.users[room.now]);
				//Room.view.whoNow(room.users[room.now]);
				Room.bind.map();

				Room.game.started = true;
				Room.game.currentPlayer = room.users[room.now];
			},
			hit: function(room) {
				for(var i = 0; i < 3; i++) {
					for(var j = 0; j < 3; j++) {
						Room.view.hit(i, j, room.map[i][j]);
					}
				}
				Room.game.currentPlayer = room.users[room.now];
				Room.view.whoNow(Room.game.currentPlayer);
			},
			endGame: function(room) {
				$(Room.mapItems).off('click');
				Room.view._prepareEnd(room);
				Room.view.updateUsersStatus(room.users);

				Room.game.currentPlayer = null;
				Room.game.started = false;
			}
		}
	},

	game: {
		player: null,
		room: null,
		currentPlayer: null,
		started: false
	}
};