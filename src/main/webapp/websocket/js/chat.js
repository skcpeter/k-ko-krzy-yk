var Chat = {
	content: '.chat',
	input: '.message',
	init: function() {
		console.log('init');
		$(Chat.input).keyup(function(e) {
			// enter 
			if(e.keyCode === 13) { 
				var text = $(this).val();
				console.log(Bristleback.USER_CONTEXT);
				Game.serverHandler.chatActionClass.sendMessage(text, Bristleback.USER_CONTEXT);
				console.log('send message');
				$(this).val('');
			}
		});
	},
	addMessage: function(message, user) {
		$(Chat.content).append('<p><strong>'+user.nick+':</strong> ' + message +'</p>');
	}
};