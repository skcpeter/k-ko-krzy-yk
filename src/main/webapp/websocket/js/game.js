/* main container for our chat application */
var Game = {
  client: null,
  username: null,
  dataController: null,

  config: {
    serverUrl: "ws://10.1.1.224:8765",
    onOpen: function (event) {
      Game.serverHandler.joinGameActionClass.join(Bristleback.CONNECTOR, Game.username);
      
      console.log("Connected: " + Utils.now());
    },
    onClose: function (event) {
      View.changeScreen('login.html', true, function(){
        $(View.elements.loginPage.nick).focus();
      });
      console.log("Disconnected: " + Utils.now());
    }
  },

  serverHandler: {
    keepAliveActionClass: null,
    joinGameActionClass: null,
    roomActionClass: null,
    gameEngineActionClass: null,
    chatActionClass: null,

    prepare: {
      keepAliveActionClass: function() {
        Game.serverHandler.keepAliveActionClass = Game.dataController.getActionClass("KeepAlive");
        Game.serverHandler.keepAliveActionClass.defineAction("ping");

        setInterval(function() {
          Game.serverHandler.keepAliveActionClass.ping();
        }, 150000);
      },
      joinGameActionClass: function() {
        Game.serverHandler.joinGameActionClass = Game.dataController.getActionClass("JoinGame");
        Game.serverHandler.joinGameActionClass.defineAction("join").setResponseHandler(onLogInCallback)
          .exceptionHandler
          .setExceptionHandler("ActionValidationException", validationErrorCallback)
          .setExceptionHandler("UserExistsException", userExistsErrorCallback);

        function onLogInCallback(map) {
          View.changeScreen('waiting.html', false, function() {
            Lobby.process.loadMap(map);
          });
        }

        //callback for exceptionHandler
        function validationErrorCallback() {
          alert("Username required");
          View.changeScreen('login.html', true, function(){$(View.elements.loginPage.nick).focus();});
          Game.client.disconnect();
        }

        //callback for exceptionHandler
        function userExistsErrorCallback() {
          alert("User already exists, choose other nickname");
          View.changeScreen('login.html', true, function(){$(View.elements.loginPage.nick).focus();});
          Game.client.disconnect();
        }
      },
      roomActionClass: function() {
        Game.serverHandler.roomActionClass = Game.dataController.getActionClass("RoomAction");
        Game.serverHandler.roomActionClass.defineAction("createRoom").setResponseHandler(onCreateRoom);
        Game.serverHandler.roomActionClass.defineAction("joinToRoom").setResponseHandler(onJoinToRoom);
        Game.serverHandler.roomActionClass.defineAction("leaveRoom").setResponseHandler(onLeaveRoom);
        
        function onCreateRoom(user) {
          Room.process.server.initialize(user);
        }

        function onJoinToRoom(room) {
          Room.process.server.joinToRoom(room);
        }

        function onLeaveRoom(map) {
          View.changeScreen('waiting.html', false, function() {
            Lobby.process.loadMap(map);
          });
        }
      },
      gameEngineClass: function() {
        Game.serverHandler.gameEngineActionClass = Game.dataController.getActionClass("GameEngineAction");
        Game.serverHandler.gameEngineActionClass.defineAction("startGame");
        Game.serverHandler.gameEngineActionClass.defineAction("hit");
      },
      chatActionClass: function() {
    	  Game.serverHandler.chatActionClass = Game.dataController.getActionClass("ChatAction");
        Game.serverHandler.chatActionClass.defineAction("sendMessage");
      },
      actions: function() {
        Game.serverHandler.prepare.joinGameActionClass();
        Game.serverHandler.prepare.roomActionClass();
        Game.serverHandler.prepare.gameEngineClass();
        Game.serverHandler.prepare.chatActionClass();
      },
    }
  },

  clientHandler: {
    userClientActionClass: {
      addUser: function (user) {
        Lobby.process.addUser(user);
      },
      removeUser: function(user) {
        Lobby.process.removeUser(user);
        if(user.room) {
          Room.view.removeUser(user);
        }
      }
    },

    roomClientActionClass: {
      createRoom: function(room) {
        Lobby.process.addRoom(room);
      },
      updateRoom: function(room) {
        Room.process.client.updateRoom(room);
      },
      removeRoom: function(rooms) {
        Lobby.process.removeRoom(rooms);
      },
      updateLobby: function(room) {
        Lobby.process.updateRoom(room);
      },
      removeUserFromRoom: function(room) {
        Room.process.client.removeUser(room);
      },
      updateUsersScore: function(room) {
        Lobby.process.updateScore(room.users);
      }
    },

    gameEngineClientActionClass: {
      startGame: function(room) {
        Room.process.client.startGame(room);
      },
      hit: function(room) {
        Room.process.client.hit(room);
      },
      endGame: function(room) {
        Room.process.client.endGame(room);
      }
    },
    
    chatClientActionClass: {
      addMessage: function(message, user) {
        Chat.addMessage(message, user);
      }
    },

    prepare: {
      global: function(){
        Game.client = Bristleback.newClient(Game.config);
        Game.dataController = Game.client.dataController;
      },
      actions: function() {
        Game.dataController.registerClientActionClass("UserClientAction", Game.clientHandler.userClientActionClass);
        Game.dataController.registerClientActionClass("RoomClientAction", Game.clientHandler.roomClientActionClass);
        Game.dataController.registerClientActionClass("GameEngineClientAction", Game.clientHandler.gameEngineClientActionClass);
        Game.dataController.registerClientActionClass("ChatClientAction", Game.clientHandler.chatClientActionClass);
      }
    }
  },
};

var View = {
  elements: {
    container: $('.content'),
    loginPage: {
      button: '#login',
      nick: '#nick'
    },
    waitingPage: {
      userList: '.user-list',
      button: '#createRoom',
      name: '#name'
    }
  },

  init: function() {
    View.changeScreen('login.html', false, function(){
      $(View.elements.loginPage.nick).focus();}
    );
    View.document();
  },
  document: function() {
    if(Game.client && Game.client.isConnected()) {
      Game.client.disconnect();
    }
    View.elements.container.on("keyup", View.elements.loginPage.nick, View.keyup.login);
    View.elements.container.on('click', View.elements.loginPage.button, View.process.login);
    View.elements.container.on('click', View.elements.waitingPage.button, View.process.createRoom);
  },

  keyup: {
    login: function(e) {
      if(e.keyCode == 13) {
        View.process.login();
      }
    },
    room: function(e) {
      if(e.keyCode == 13) {
        View.process.login();
      }
    }
  },
  process: {
    login: function() {
      var nick = $(View.elements.loginPage.nick).val();

      Game.username = nick;  //Game.username field is used in onOpen function
      Game.client.connect(); //open web socket connection, onOpen function will be invoket as result

      if(Game.serverHandler.keepAliveActionClass === null)
        Game.serverHandler.prepare.keepAliveActionClass();
    },
    createRoom: function() {
      var name = $(View.elements.waitingPage.name).val();

      if(Game.serverHandler.roomActionClass !== null) {
        Game.serverHandler.roomActionClass.createRoom(Bristleback.USER_CONTEXT, name);
      } else {
        console.log('ops... something was wrong');
      }
    }
  },
  changeScreen: function(url, instant, callback) {
    if($.isFunction(callback) === false) {
      callback = function() {}
    }

    $.post(url, null, function(response) {
      View.newContent = $(response).hide();
      View.elements.container.html(View.newContent);

      if(instant === true) {
        View.elements.container.stop(true, true).show().html(View.newContent.show()).show();
        callback();
      } else {
        View.elements.container.stop(true, true).fadeOut(100,function() {
          $(this).html(View.newContent.show());
          $(this).fadeIn(100, callback);
        });
      }
    });
  }
};

var Utils = {
  now: function() {
    var formattedDate = new Date();
    var h = formattedDate.getHours();
    var m =  formattedDate.getMinutes();
    var s = formattedDate.getSeconds();

    if(h < 10) {
      h = '0' + h;
    }
    if(m < 10) {
      m = '0' + m;
    }
    if(s < 10) {
      s = '0' + s;
    }
    
    return (h + ':' + m + ':' + s);
  }
};