var Lobby = {
   elements: {
    container: '#lobby',
    userList: '#lobby .user-list',
    button: '#lobby #createRoom',
    name: '#lobby #name',
    roomContainer: '#lobby .room-list',
    roomList: '#lobby .room-list .room-item:not(.room-item-template)',
    template: '#lobby .room-item-template',
    roomContent: '#lobby #room-content'
  },

  status: {
    live: 'room-live',
    busy: 'room-busy',
    away: 'room-away'
  },

  view: {
    _tooltip: function(user) {
      return $('<div class="tooltip"><p>Wygrane: '+user.win+'</p><p>Przegrane: '+user.lost+'</p><p>Remisy: '+user.draw+'</p></div>');
    },
    _roomStatus: function(template, status) {
      template.attr('class', 'room-item ' + status)
    },
    _clearUsers: function() {
      $(Lobby.userList).html('');
    },
    _template: function(template, room) {
      template
        .find('.text-header-block')
          .find('.room-name')
            .html(room.name)
          .end()
        .end()
        .find('.room-players')
          .html('<span>'+ room.users[0].nick +'</span>')
        .end()
        .attr('id', 'room-'+room.id);

      if(room.users.length === 1) {
        Lobby.view._roomStatus(template, Lobby.status.live);
      } else if(room.users.length === 2) {
        template
          .find('.room-players')
            .append('<span >'+ room.users[1].nick +'</span>');

        Lobby.view._roomStatus(template, Lobby.status.busy);
      }
    }
  },

  bind: {
    joinRoom: function(el, roomId) {
      el.click(function(e) {
        if(el.hasClass(Lobby.status.live)) {
          Game.serverHandler.roomActionClass.joinToRoom(Bristleback.USER_CONTEXT, roomId);
        } else {
          console.log('Room is full');
        }
      });
    }
  },

  process: {
    addRoom: function(room) {
      var template = $(Lobby.elements.template).clone().removeClass('room-item-template');

      Lobby.view._template(template, room);
      $(Lobby.elements.roomContainer).append(template.hide().fadeIn(100,function() {
        Lobby.bind.joinRoom(template, room.id);
      }));

    },

    updateRoom: function(room) {
      var template = $('#room-' + room.id);
      Lobby.view._template(template, room);
    },

    addUser: function(user) {
      var item = $('<li>'+ user.nick +'</li>')
                      .attr('id', 'user-'+user.id)
                      .append(Lobby.view._tooltip(user))
                      .hide();

      $(Lobby.elements.userList).append(item.fadeIn(100));
    },

    updateScore: function(users) {
      $.each(users, function(k, user) {
        var tooltip = Lobby.view._tooltip(user);
        $('#user-'+user.id).find('.tooltip').replaceWith(tooltip);
      });
    },

    loadMap: function(map) {
      Lobby.process.loadUsers(map.users);
      Lobby.process.loadRooms(map.rooms);
      Chat.init();
    },

    loadUsers: function(users) {
      Lobby.view._clearUsers();

      for(var i = 0; i < users.length; i++) {
        Lobby.process.addUser(users[i]);
      }
    },

    loadRooms: function(rooms) {
      for(var i = 0; i < rooms.length; i++) {
        Lobby.process.addRoom(rooms[i]);
      }
    },

    removeRoom: function(rooms) {
      if(rooms.length === 0) {
        $(Lobby.elements.roomList).each(function() {
          $(this).fadeOut(100,function() {
            $(this).remove();
          });
        });
      } else {
        $(Lobby.elements.roomList).each(function() {
          var id = $(this).attr('id');
          var exists = false;
          $.each(rooms, function(k, room) {
            if(id === ('room-' + room.id))
              exists = true;
          });
          if(!exists) {
            $('#'+id).fadeOut(100,function() {
              $(this).remove();
            });
          }
        });
      }
    },
    
    removeUser: function(user) {
      $('#user-' + user.id).fadeOut(100,function() { $(this).remove() });
    }
  },
};